const express = require("express"),
  Router = express.Router(),
  getDataFromAllProviders = require('../services/getDataFromAllProviders'),
  setCombineFligths = require('../services/setCombineFligths'),
  validateFligths = require('../validators/fligths'),
  groupByPriceR = require('../services/R/groupByPriceR');

Router.get(
  "/flights",
  async (req, res) => {
    const fligth = req.query;
    const { error } = validateFligths(fligth);
    if (error) return res.status(400).send(error.details);
    let flights;
    /* OW */
    if (fligth.tripType === "OW") {
      await getDataFromAllProviders(fligth.departure_airport, fligth.arrival_airport, fligth.departure_date)
        .then(async data =>
          flights = await setCombineFligths(data, "OW")
        ).catch(err => {
          res.status(400).json({ err, message: "erreur", ok: "failure" });
        })
    }
    /* R */
    else {
      try {
        flights = await groupByPriceR(fligth);
      }
      catch { err => res.status(400).json({ err, message: "erreur", ok: "failure" }) }
    }
    res.status(200).json({
      ok: "success",
      msg: "get flights wih success",
      params: fligth,
      data: flights
    })
  }
);

module.exports = Router;
const Joi = require("joi");

function validateFligths(fligth) {
    const schema = Joi.object().keys({
        departure_airport: Joi.string().max(3).min(3).required(),
        arrival_airport: Joi.string().max(3).min(3).required(),
        departure_date: Joi.date().required(),
        return_date: Joi.date().optional().allow(''),
        tripType: Joi.string().valid('R', 'OW').uppercase().required()
    });
    return Joi.validate( fligth , schema);
}
module.exports = validateFligths;

const axios = require('axios');

module.exports = async function getDataFromAllProviders(param1, param2, param3) {
    var dataProvider1 = [];
    var dataProvider2 = [];
    try {
        // get reequest
        dataProvider1 = await axios.get(`http://flights.beta.bcmenergy.fr/jazz/flights?departure_airport=${param1}&arrival_airport=${param2}&departure_date=${param3}`);
        dataProvider2 = await axios.get(`http://flights.beta.bcmenergy.fr/moon/flights?departure_airport=${param1}&arrival_airport=${param2}&departure_date=${param3}`);
    }
    catch { err => console.log(err); }
    const setArrayLegs = dataProvider2.data.map(flight => {
        return {
            price: flight.price,
            flight: flight.legs[0]
        }
    });
    const globalData = [...setArrayLegs, ...dataProvider1.data]
    return globalData;
};
const _ = require('lodash'),
    groupByOutbound = require('./R/groupByOutbound')

module.exports = async function setCombineFligths(flights, typeFligth) {
    let result;
    if (typeFligth === 'OW') {
        result = _.chain(flights)
            .groupBy("price")
            .map((value, key) => ({ price: key, combinations: value.map(elem => elem.flight) }))
            .value()
    }
    else {
        let groupedFlights = _.groupBy(flights, "price");
        result = Object.keys(groupedFlights).map(item => {
            return {
                price: item,
                combinations: groupByOutbound(groupedFlights[item])
            }
        })
    }
    return result;
};
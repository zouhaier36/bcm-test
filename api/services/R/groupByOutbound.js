const _ = require('lodash')
module.exports = function groupByOutbound(arr) {
    let groupedFlightsOutbound = [];
    if (arr.length === 1) {
        groupedFlightsOutbound.push({
            outbound: arr[0].outbound,
            inbound: [arr[0].inbound]
        })
    }
    else {
        let arrayOfOutbound = arr.map(flight => {
            return flight.outbound
        })
        const uniqArrayOfOutbound = _.uniqBy(arrayOfOutbound, 'id');
        let arrayOfInbound = arr.map(flight => {
            return flight.inbound
        })
        uniqArrayOfOutbound.forEach(element => {
            groupedFlightsOutbound.push({
                outbound: element,
                inbound: arrayOfInbound.filter(f => f.idDepart === element.id)
            })
        });
    }
    return groupedFlightsOutbound;
}
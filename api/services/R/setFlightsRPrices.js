module.exports = async function setFlightsRPrices(departFlights, returnFligths) {
    const CombineFlights = [];
    for (let i = 0; i < departFlights.length; i++) {
        // filter by arrival_time 
        let filtredFlights = returnFligths.filter(flight => new Date(departFlights[i].flight.arrival_time) < new Date(flight.flight.departure_time))
        filtredFlights.forEach(flight => {
            CombineFlights.push({
                price: departFlights[i].price + flight.price,
                outbound: { ...departFlights[i].flight, price: departFlights[i].price },
                inbound: { ...flight.flight, price: flight.price, idDepart: departFlights[i].flight.id }
            })
        }
        )
    }
    return CombineFlights;
}
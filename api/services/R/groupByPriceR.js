const getDataFromAllProviders = require('../getDataFromAllProviders'),
    setCombineFligths = require('../setCombineFligths'),
    setFlightsRPrices = require('./setFlightsRPrices');

module.exports = async function groupByPriceR(flight) {
    // We search for a one way trip (with only the outgoing information)
    const departFlights = await getDataFromAllProviders(flight.departure_airport, flight.arrival_airport, flight.departure_date);
    //We search for a one way trip (with only the returning information)
    const returnFligths = await getDataFromAllProviders(flight.arrival_airport, flight.departure_airport, flight.return_date);
    const CombineFlights = await setFlightsRPrices(departFlights, returnFligths);
    //We combine all the one ways into returns flights
    const dataCombined = await setCombineFligths(CombineFlights);
    return dataCombined;
};
/**
 * Main application file
 */

"use strict";

const express = require("express"),
    config = require("config"),
    port = process.env.PORT || config.get("port"),
    bodyParser = require("body-parser"),
    app = express();

// load app middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require("./middleware/cors")(app);

// load our API routes

app.use("/api", require("./api/controllers/flights"));

//connect to server 

app.listen(port, (err) => {
    if (err)
        console.log(err);
    console.log("server is connected on port:", port);
});
